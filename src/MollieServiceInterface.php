<?php

namespace Drupal\mollie_donations;

/**
 * Interface for the MollieService.
 */
interface MollieServiceInterface {

  /**
   * Create a Mollie payment.
   *
   * @param string $amount
   *   The price of the payment.
   * @param array $description
   *   The description.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Mollie\Api\Exceptions\ApiException
   * @throws \Mollie\Api\Exceptions\IncompatiblePlatform
   */
  public function createMolliePayment($amount, array $description);

  /**
   * Get the payment object.
   *
   * @return \Mollie\Api\Resources\Payment|null
   *   A molliePayment object, or null if it could not be found.
   */
  public function getPayment();

}
