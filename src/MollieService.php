<?php

namespace Drupal\mollie_donations;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Api\MollieApiClient;

/**
 * The service that connects to Mollie.
 */
class MollieService implements MollieServiceInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Drupal\Core\Session\SessionManagerInterface definition.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Drupal\Core\PageCache\ResponsePolicy\KillSwitch definition.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Drupal\Core\Session\AccountInterface definition.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Mollie API.
   *
   * @var \Mollie\Api\MollieApiClient
   */
  protected $mollie;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * If we are in debug mode.
   *
   * @var array|mixed|null
   */
  public $isDebug;

  /**
   * The Mollie payment object.
   *
   * @var \Mollie\Api\Resources\Payment
   */
  public $payment;

  /**
   * Constructs a new MollieService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, KillSwitch $kill_switch, AccountInterface $current_user, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager = $session_manager;
    $this->killSwitch = $kill_switch;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory;

    $this->config = $this->configFactory->get('mollie_donations.settings');
    $this->isDebug = $this->config->get('debug_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function createMolliePayment($amount, array $description) {
    $this->setDebugMessage('createMolliePayment');

    if (!isset($_SESSION['session_started']) && $this->currentUser->isAnonymous()) {
      $_SESSION['session_started'] = TRUE;
      $this->sessionManager->regenerate();
    }

    $currency = (!empty($this->config->get('donation_currency'))) ? $this->config->get('donation_currency') : 'EUR';

    global $base_url;
    $this->setMollie();

    // Clean the amount value.
    $amount = $this->cleanAmount($amount, $currency);

    // Create the mollie data.
    $mollieArray = [
      'amount' => [
        'currency' => $currency,
        'value' => $amount,
      ],
      'description' => implode(' | ', $description),
      'redirectUrl' => $base_url . '/mollie_donations/callback',
      'metadata' => ['order_id' => time()],
    ];

    $this->setDebugMessage('mollieArray: ' . print_r($mollieArray, 1));

    try {
      $this->payment = $this->mollie->payments->create($mollieArray);
      setcookie('molliePayment', json_encode($this->payment));

      $response = new TrustedRedirectResponse(Url::fromUri($this->payment->getCheckoutUrl())
        ->toString());

      $metadata = $response->getCacheableMetadata();
      $metadata->setCacheMaxAge(0);

      $response->send();
    }
    catch (ApiException $exception) {
      $this->messenger->addError($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPayment() {
    $this->killSwitch->trigger();
    $this->setMollie();
    $status = 'unknown';
    if (isset($_COOKIE['molliePayment'])) {
      $oldPayment = json_decode($_COOKIE['molliePayment']);
      setcookie('molliePayment', NULL, -1);
      $payment = $this->mollie->payments->get($oldPayment->id);
      $this->setDebugMessage('payment: ' . print_r($payment, 1));
      $status = $payment->status;
    }

    $uri = Url::fromRoute('mollie_donations.donate', ['status' => $status]);
    $this->setDebugMessage('redirect uri: ' . $uri->toString());
    $response = new TrustedRedirectResponse($uri->toString());
    $response->send();

    return $payment ?? NULL;
  }

  /**
   * Create the Mollie api client.
   *
   * @throws \Mollie\Api\Exceptions\ApiException
   * @throws \Mollie\Api\Exceptions\IncompatiblePlatform
   */
  protected function setMollie() {
    $this->mollie = new MollieApiClient();
    $this->setMollieApiKey();
  }

  /**
   * Set the API key based on the live payments setting.
   *
   * @throws \Mollie\Api\Exceptions\ApiException
   */
  protected function setMollieApiKey() {
    $isOnline = $this->config->get('mollie_live_payments');
    if (!empty($isOnline)) {
      $this->mollie->setApiKey($this->config->get('mollie_live_api_key'));
    }
    else {
      $this->mollie->setApiKey($this->config->get('mollie_test_api_key'));
    }
  }

  /**
   * Clean the amount value so Mollie can use it.
   *
   * @param string $amount
   *   The amount to donate.
   * @param string $currency
   *   The currency type.
   *
   * @return string
   *   The cleaned amount.
   */
  protected function cleanAmount($amount, $currency) {
    // Convert amount.
    $amount = str_replace(',', '.', $amount);

    // Japanese Yen doesn't have decimals. For others, we need to add them if
    // they don't exist.
    if ($currency !== 'JPY') {
      $amountSplit = explode('.', $amount);
      if (count($amountSplit) === 1) {
        $amount .= '.00';
      }
    }

    return $amount;
  }

  /**
   * Write a debug message in the dblog.
   *
   * @param string $message
   *   The message to show in the dblog.
   */
  protected function setDebugMessage($message) {
    if ($this->isDebug) {
      $this->loggerFactory->get('mollie_donations')
        ->notice($message);
    }
  }

}
