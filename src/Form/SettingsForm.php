<?php

namespace Drupal\mollie_donations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for the mollie_donations module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mollie_donations.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mollie_donations.settings');

    $form['mollie_live_payments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Live payments'),
      '#default_value' => $config->get('mollie_live_payments'),
    ];

    $form['activate_donations'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate donations'),
      '#default_value' => $config->get('activate_donations'),
    ];

    $form['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug messages in the dblog'),
      '#default_value' => $config->get('debug_mode'),
    ];

    $form['mollie_live_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mollie API key'),
      '#default_value' => $config->get('mollie_live_api_key'),
      '#required' => TRUE,
    ];

    $form['mollie_test_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mollie API test key'),
      '#default_value' => $config->get('mollie_test_api_key'),
      '#required' => TRUE,
    ];

    $form['donation_amounts'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Donation amounts'),
      '#description' => $this->t('A semicolon separated list of donation amounts that one can choose when donating. For example, &quot;2,00; 5,00; 10,00; 25,00;50,00&quot;'),
      '#default_value' => $config->get('donation_amounts'),
      '#required' => TRUE,
    ];

    $form['donation_amounts_other'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable "other" donation amount'),
      '#default_value' => ($config->get('donation_amounts_other') !== NULL) ? $config->get('donation_amounts_other') : TRUE,
    ];

    $form['donation_currency_symbol'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Donation currency symbol'),
      '#default_value' => $config->get('donation_currency_symbol'),
      '#required' => TRUE,
    ];

    $form['donation_currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Donation currency'),
      '#default_value' => (!empty($config->get('donation_currency'))) ? $config->get('donation_currency') : 'EUR',
      '#options' => $this->getCurrencies(),
      '#required' => TRUE,
    ];

    $form['donation_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page path'),
      '#description' => $this->t('For instance, donations. Do NOT include the /'),
      '#default_value' => $config->get('donation_path'),
      '#required' => TRUE,
    ];

    $form['donation_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Donation page title'),
      '#default_value' => $config->get('donation_page_title'),
      '#required' => TRUE,
    ];

    $text = $config->get('donation_page_text');
    $form['donation_page_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text on top of the donation form'),
      '#default_value' => (isset($text['value'])) ? $text['value'] : '',
      '#format' => (isset($text['format'])) ? $text['format'] : '',
    ];

    $form['donation_thank_you'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message text after successful donation'),
      '#default_value' => $config->get('donation_thank_you'),
    ];

    $form['donation_went_wrong'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message text after failed donation'),
      '#default_value' => $config->get('donation_went_wrong'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Invalidate the cache for this user.
    drupal_flush_all_caches();

    $this->config('mollie_donations.settings')
      ->set('mollie_live_payments', $form_state->getValue('mollie_live_payments'))
      ->set('debug_mode', $form_state->getValue('debug_mode'))
      ->set('mollie_live_api_key', $form_state->getValue('mollie_live_api_key'))
      ->set('mollie_test_api_key', $form_state->getValue('mollie_test_api_key'))
      ->set('donation_path', $form_state->getValue('donation_path'))
      ->set('donation_amounts', $form_state->getValue('donation_amounts'))
      ->set('donation_amounts_other', $form_state->getValue('donation_amounts_other'))
      ->set('donation_currency_symbol', $form_state->getValue('donation_currency_symbol'))
      ->set('donation_currency', $form_state->getValue('donation_currency'))
      ->set('donation_page_title', $form_state->getValue('donation_page_title'))
      ->set('donation_page_text', $form_state->getValue('donation_page_text'))
      ->set('donation_thank_you', $form_state->getValue('donation_thank_you'))
      ->set('donation_went_wrong', $form_state->getValue('donation_went_wrong'))
      ->set('activate_donations', $form_state->getValue('activate_donations'))
      ->save();
  }

  /**
   * Get the list of available currencies that Mollie supports.
   *
   * @return string[]
   *   The list of currencies.
   */
  protected function getCurrencies() {
    return [
      'AED' => $this->t('United Arab Emirates dirham'),
      'AUD' => $this->t('Australian dollar'),
      'BGN' => $this->t('Bulgarian lev'),
      'BRL' => $this->t('Brazilian real'),
      'CAD' => $this->t('Canadian dollar'),
      'CHF' => $this->t('Swiss franc'),
      'CZK' => $this->t('Czech koruna'),
      'DKK' => $this->t('Danish krone'),
      'EUR' => $this->t('EURO'),
      'GBP' => $this->t('British pound'),
      'HKD' => $this->t('Hong Kong dollar'),
      'HRK' => $this->t('Croatian kuna'),
      'HUF' => $this->t('Hungarian forint'),
      'ILS' => $this->t('Israeli new shekel'),
      'ISK' => $this->t('Icelandic króna'),
      'JPY' => $this->t('Japanese yen'),
      'MXN' => $this->t('Mexican peso'),
      'MYR' => $this->t('Malaysian ringgit'),
      'NOK' => $this->t('Norwegian krone'),
      'NZD' => $this->t('New Zealand dollar'),
      'PHP' => $this->t('Philippine piso'),
      'PLN' => $this->t('Polish złoty'),
      'RON' => $this->t('Romanian leu'),
      'RUB' => $this->t('Russian ruble'),
      'SEK' => $this->t('Swedish krona'),
      'SGD' => $this->t('Singapore dollar'),
      'THB' => $this->t('Thai baht'),
      'TWD' => $this->t('New Taiwan dollar'),
      'USD' => $this->t('United States dollar'),
      'ZAR' => $this->t('South African rand'),
    ];
  }

}
