<?php

namespace Drupal\mollie_donations\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\mollie_donations\MollieServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The actual donation form.
 */
class DonationForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Config\ImmutableConfig definition.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\mollie_donations\MollieServiceInterface definition.
   *
   * @var \Drupal\mollie_donations\MollieServiceInterface
   */
  protected $mollieService;

  /**
   * Constructs a new DonationForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MollieServiceInterface $mollieService, AccountInterface $account, RequestStack $request_stack) {
    $this->configFactory = $config_factory;
    $this->mollieService = $mollieService;
    $this->currentUser = $account;
    $this->requestStack = $request_stack;

    $this->config = $this->configFactory->get('mollie_donations.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'), $container->get('mollie_donations.mollie'), $container->get('current_user'), $container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'donation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->showMessage();

    $topText = $this->config->get('donation_page_text');
    if (!empty($topText)) {
      $form['top_text'] = [
        '#markup' => check_markup($topText['value'], $topText['format']),
      ];
    }

    $form['donation_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
    ];

    $form['donation_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#required' => TRUE,
    ];

    $amounts = $this->config->get('donation_amounts');
    $amountsOther = ($this->config->get('donation_amounts_other') !== NULL) ? $this->config->get('donation_amounts_other') : TRUE;

    $currency = (!empty($this->config->get('donation_currency_symbol'))) ? $this->config->get('donation_currency_symbol') : '€';
    $values = explode(';', $amounts);
    $options = [];
    foreach ($values as $value) {
      $value = trim($value);
      $options[$value] = $currency . ' ' . $value;
    }

    if (!empty($amountsOther)) {
      $options['other'] = t('Other amount');
    }

    $form['donation_amount'] = [
      '#type' => 'select',
      '#title' => $this->t('Donation amount'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    if (!empty($amountsOther)) {
      $form['other_amount'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Other amount'),
        '#states' => [
          'visible' => [
            ':input[name="donation_amount"]' => ['value' => 'other'],
          ],
        ],
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Donate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $description = [
      $values['donation_name'],
      $values['donation_email'],
    ];

    $amount = $values['donation_amount'];
    if ($amount === 'other') {
      $amount = $values['other_amount'];
    }

    $this->mollieService->createMolliePayment($amount, $description);
  }

  /**
   * Set the page title of the form.
   */
  public function title() {
    return $this->config->get('donation_page_title');
  }

  /**
   * Custom access check for the form and callback.
   */
  public function access() {
    $isActive = $this->config->get('activate_donations');

    return AccessResult::allowedIf(!empty($isActive) || $this->currentUser->isAuthenticated());
  }

  /**
   * Show a message after payment.
   */
  protected function showMessage() {
    $status = $this->requestStack->getCurrentRequest()
      ->get('status');
    if ($status !== NULL) {
      $this->messenger()->deleteAll();
      if ($status === 'paid') {
        $this->messenger()
          ->addMessage($this->config->get('donation_thank_you'));
      }
      else {
        $this->messenger()
          ->addError($this->config->get('donation_went_wrong'));
      }
    }
  }

}
