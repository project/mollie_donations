<?php

namespace Drupal\mollie_donations\Routing;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Routing for the Mollie Donations.
 */
class MollieDonationsRouting implements ContainerInjectionInterface {

  /**
   * The modules config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->config = $configFactory->get('mollie_donations.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $routes = [];

    $path = (!empty($this->config->get('donation_path'))) ? $this->config->get('donation_path') : 'donate';
    $routes['mollie_donations.donate'] = new Route($path, [
      '_form' => '\Drupal\mollie_donations\Form\DonationForm',
      '_title_callback' => '\Drupal\mollie_donations\Form\DonationForm::title',
    ], [
      '_custom_access' => '\Drupal\mollie_donations\Form\DonationForm::access',
    ]);
    return $routes;
  }

}
