<?php

namespace Drupal\mollie_donations\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DonateBlock' block.
 *
 * @Block(
 *  id = "donate_block",
 *  admin_label = @Translation("Donate block"),
 * )
 */
class DonateBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('mollie_donations.settings');
    $isActive = $config->get('activate_donations');

    if (!empty($isActive)) {
      return [
        '#title' => t('Help us! Donate online here!'),
        '#type' => 'link',
        '#url' => Url::fromRoute('mollie_donations.donation_form'),
        '#cache' => [
          'contexts' => ['user'],
        ],
      ];
    }

    return [];
  }

}
