<?php

namespace Drupal\mollie_donations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\mollie_donations\MollieServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the Mollie callback.
 */
class MollieCallbackController extends ControllerBase {

  /**
   * Drupal\mollie_donations\MollieServiceInterface definition.
   *
   * @var \Drupal\mollie_donations\MollieServiceInterface
   */
  protected $mollieService;

  /**
   * Drupal\Core\PageCache\ResponsePolicy\KillSwitch definition.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Constructs a new MollieCallbackController object.
   */
  public function __construct(MollieServiceInterface $mollie_service, KillSwitch $kill_switch) {
    $this->mollieService = $mollie_service;
    $this->killSwitch = $kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('mollie_donations.mollie'), $container->get('page_cache_kill_switch'));
  }

  /**
   * Callback.
   *
   * @return array
   *   Method return.
   */
  public function callback() {
    $this->killSwitch->trigger();
    $this->mollieService->getPayment();

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: callback'),
    ];
  }

}
