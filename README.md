CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Roadmap
* Maintainers


INTRODUCTION
------------

This module creates a form that makes it possible to make donations with the Mollie API.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/mollie_donations

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/mollie_donations


REQUIREMENTS
------------

* Mollie PHP Api, required via composer.json.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

Configuration can be done on `admin/config/services/mollie_donations`.

Roadmap
-------

* List of all donations
* More?


MAINTAINERS
-----------

Current maintainers:
* Fabian de Rijk (fabianderijk) - https://www.drupal.org/u/fabianderijk
